# Routes
| Method    | Path  | Body              | Response          |  
|-----------|-------|-------------------|-------------------|
| GET       | /:key |                   | { value: string } |  
| POST      | /:key | { value: string } | { value: string } |

# Env Vars

| Name              | Example                       |
|-------------------|-------------------------------|
|REDIS_URL          |redis://localhost:6379         |
|PORT               |3000                           |
|OTHER_SERVICE_URL  |http://localhost:3001          |