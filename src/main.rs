use std::{collections::HashMap, sync::Arc};

use axum::{extract::{Path, State}, routing::get, Json, Router};
use redis::{aio::MultiplexedConnection, AsyncCommands};
use reqwest;
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;
#[derive(Deserialize, Debug)]
struct Config {
    redis_url: String,
    other_service_url: String,
    port: u64,
}

#[derive(Clone)]
struct AppState {
    redis_connection: Arc<Mutex<MultiplexedConnection>>,
    other_service: Arc<OtherService>,
}

#[tokio::main]
async fn main() {
    let config = match envy::from_env::<Config>() {
        Ok(config) => config,
        Err(error) => panic!("Environment {:#?}", error),
    };

    let client = redis::Client::open(config.redis_url).unwrap();
    let redis_connection = client.get_multiplexed_async_connection().await.unwrap();
    let state = AppState {
        redis_connection: Arc::new(Mutex::new(redis_connection)),
        other_service: Arc::new(OtherService {
            base_path: config.other_service_url,
        }),
    };

    // build our application with a single route
    let app = Router::new()
        .route("/:key", get(handle_get_request).post(handle_set_request))
        .with_state(state);

    // run our app with hyper, listening globally on port 3000
    let listener = tokio::net::TcpListener::bind(format!("0.0.0.0:{}", config.port))
        .await
        .unwrap();
    println!("Started server on port {}", config.port);
    axum::serve(listener, app).await.unwrap();
}

#[derive(Serialize)]

struct OptionalValue {
    value: Option<String>,
}
#[derive(Deserialize)]

struct Value {
    value: String,
}

async fn handle_get_request(
    State(state): State<AppState>,
    Path(key): Path<String>,
) -> Json<OptionalValue> {
    let value = match state
        .redis_connection
        .lock()
        .await
        .get::<&String, String>(&key)
        .await
    {
        Ok(value) => Some(value),
        _ => None,
    };

    return Json(OptionalValue { value });
}

async fn handle_set_request(
    State(state): State<AppState>,
    Path(key): Path<String>,
    Json(payload): Json<Value>,
) -> Json<OptionalValue> {
    state
        .redis_connection
        .lock()
        .await
        .set::<&String, String, String>(&key, payload.value)
        .await
        .expect("Unable to write value to database");

    let value = state.other_service.get_value(key).await;
    return Json(OptionalValue { value });
}

struct OtherService {
    base_path: String,
}

impl OtherService {
    async fn get_value(&self, key: String) -> Option<String> {
        let response = reqwest::get(format!("{}/{}", self.base_path, key))
            .await
            .unwrap()
            .json::<HashMap<String, String>>()
            .await
            .unwrap();
        match response.get("value") {
            Some(value) => Some(value.clone()),
            None => None,
        }
    }
}
