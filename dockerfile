FROM rust as builder
WORKDIR /build
COPY . .
RUN cargo build --release

FROM scratch AS runtime
WORKDIR /app
COPY --from=builder /build/target/release/api /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/app"]